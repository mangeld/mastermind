from mastermind.code_maker import RandomCodeMaker
from mastermind.utils import Random
from mastermind.rules import Rules


class TestCreateCode:

    def test_secret_code_has_4_pegs(self):
        code = RandomCodeMaker(Rules(), Random).create_code()
        assert len(code.pegs) == 4

    def test_same_seed_returns_same_pegs(self):
        Random.seed(12)
        code_a = RandomCodeMaker(Rules(), Random).create_code()
        Random.seed(12)
        code_b = RandomCodeMaker(Rules(), Random).create_code()

        colors_a = [i.color for i in code_a.pegs]
        colors_b = [i.color for i in code_b.pegs]

        assert colors_a == colors_b

    def test_different_seed_returns_different_pegs(self):
        Random.seed(12)
        code_a = RandomCodeMaker(Rules(), Random).create_code()
        Random.seed(234)
        code_b = RandomCodeMaker(Rules(), Random).create_code()

        colors_a = [i.color for i in code_a.pegs]
        colors_b = [i.color for i in code_b.pegs]

        assert colors_a != colors_b

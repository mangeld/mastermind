from pytest import raises, mark

from mastermind.board import Board, Rules
from mastermind.code_maker import StaticCodeMaker
from mastermind.peg import Peg
from mastermind.errors import MaxMovesReached


class TestBoard:

    def test_game_is_won(self):
        red_pegs = [Peg("RED") for i in range(4)]
        maker = StaticCodeMaker(Rules(), red_pegs)
        board = Board(maker, Rules())

        assert board.guess(red_pegs)

    def test_board_tells_if_game_is_won(self):
        red_pegs = [Peg("RED") for i in range(4)]
        maker = StaticCodeMaker(Rules(), red_pegs)
        board = Board(maker, Rules())
        board.guess(red_pegs)

        assert board.won is True

    def test_one_move_game_is_not_won(self):
        red_pegs = [Peg("RED") for i in range(4)]
        blue_pegs = [Peg("BLUE") for i in range(4)]
        maker = StaticCodeMaker(Rules(), red_pegs)
        board = Board(maker, Rules())
        board.guess(blue_pegs)

        assert board.won is False

    def test_max_moves_reached(self):
        rules = Rules()
        red_pegs = [Peg("RED") for i in range(rules.played_pegs)]
        blue_pegs = [Peg("BLUE") for _ in range(rules.played_pegs)]
        maker = StaticCodeMaker(rules, blue_pegs)
        board = Board(maker, rules)

        with raises(MaxMovesReached):
            for _ in range(rules.max_guesses + 1):
                board.guess(red_pegs)

    def test_game_won_in_last_try(self):
        rules = Rules()
        red_pegs = [Peg("RED") for i in range(rules.played_pegs)]
        blue_pegs = [Peg("BLUE") for _ in range(rules.played_pegs)]
        maker = StaticCodeMaker(rules, blue_pegs)
        board = Board(maker, rules)

        for _ in range(rules.max_guesses - 1):
            board.guess(red_pegs)

        board.guess(blue_pegs)
        assert board.won

    @mark.parametrize('secret, guess, expectation', [
        (
            ['BLACK', 'BROWN', 'RED', 'YELLOW'],
            ['RED', 'BLACK', 'BLACK', 'BLACK'],
            [False, False, None, None]
        ),
        (
            ['BLACK', 'BROWN', 'RED', 'YELLOW'],
            ['GREEN', 'BLACK', 'RED', 'RED'],
            [True, False, None, None]
        ),
        (
            ['RED', 'BLUE', 'YELLOW', 'ORANGE'],
            ['WHITE', 'BLACK', 'BLUE', 'BROWN'],
            [False, None, None, None]
        ),
        (
            ['YELLOW', 'WHITE', 'RED', 'RED'],
            ['RED', 'RED', 'RED', 'RED'],
            [True, True, None, None],
        ),
        (
            ['RED', 'BLUE', 'YELLOW', 'ORANGE'],
            ['WHITE', 'BLUE', 'BLACK', 'BROWN'],
            [True, None, None, None],
        ),
        (
            ['RED', 'BLUE', 'YELLOW', 'ORANGE'],
            ['RED', 'BLACK', 'BLUE', 'BROWN'],
            [True, False, None, None]
        ),
        (
            ['RED', 'BLUE', 'YELLOW', 'ORANGE'],
            ['RED', 'WHITE', 'BLACK', 'BROWN'],
            [True, None, None, None]
        ),
        (
            ['RED', 'RED', 'RED', 'RED'],
            ['BLUE', 'BLUE', 'BLUE', 'BLUE'],
            [None, None, None, None]
        )
    ])
    def test_given_guess_returns_expected_result(
            self, secret, guess, expectation):
        maker = StaticCodeMaker(Rules(), [Peg(i) for i in secret])
        guess = [Peg(i) for i in guess]
        board = Board(maker, Rules())

        assert board.guess(guess) == expectation

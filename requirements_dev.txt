-r requirements.txt
pytest==3.6.3
mypy==0.620
ipdb==0.11
pytest-cov==2.5.1
flake8==3.5.0
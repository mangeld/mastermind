# About

This project consists of a simple api built with django & django-rest-framework.

It has two main endpoints `/v1/game/` and `/v1/move/`.

Once you have created a user you can create games and make moves:

``http --auth mike:supersecret POST localhost/v1/game/``

``` json
{
    "id": 4,
    "moves": [],
    "started_on": "2018-07-23T07:52:14.875473Z",
    "won": false
}
```

Then you can make guesses on it:

`http --auth mike:supersecret POST localhost/v1/move/ game:=4 pegs:='["RED", "BLUE", "GREEN", "BROWN"]'`

``` json
{
    "game": 4,
    "index": 1,
    "pegs": [
        "RED",
        "BLUE",
        "GREEN",
        "BROWN"
    ],
    "result": [
        "WHITE",
        "RED",
        "RED",
        ""
    ]
}
```

A white peg in the result represents a correct color in the correct position 
and a red peg indicates a found color in some other position.

You can see the schema and docs in `/v1/docs/`

## How to run

Build the image:

`docker-compose build`

Run the migrations:

`docker-compose run --rm app migrate`

Create an user:

`docker-compose run --rm app manage createsuperuser`

Run the dev server:

`docker-compose run --rm --service-ports app dev`

## Run production env


`docker-compose up -d nginx`


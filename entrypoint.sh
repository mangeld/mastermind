#!/usr/bin/env sh

cd web

case $1 in
    'dev') python manage.py runserver 0.0.0.0:80 ;;
    'serve') gunicorn -c gunicorn.py web.config.wsgi;;
    'shell') python manage.py shell ;;
    'manage') shift; python manage.py $@ ;;
    'migrate') python manage.py migrate ;;
    'make' ) shift; cd /code; make $@ ;;
    * ) exec $@ ;;
esac
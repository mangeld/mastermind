from django.db.models import Model, ForeignKey, DateTimeField, CharField, CASCADE
from django.db import models
from django.contrib.auth.models import AbstractUser


class CommaSepField(models.CharField):
    "Implements comma-separated storage of lists"
    # I was going to implement this myself, but looking the
    # documentation on how to create a custom field I found this
    # example, that was just what I wanted to do. 
    # https://docs.djangoproject.com/en/2.0/howto/custom-model-fields/
    # Also I could have used an ArrayField (since I planned to use postgres)
    # but that would lock me to that RDBMS

    def __init__(self, separator=",", *args, **kwargs):
        self.separator = separator
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        # Only include kwarg if it's not the default
        if self.separator != ",":
            kwargs['separator'] = self.separator
        return name, path, args, kwargs

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value

        return value.split(self.separator)

    def to_python(self, value):
        if isinstance(value, list):
            return value

        if value is None:
            return value

        return value.split(self.separator)

    def get_prep_value(self, value):
        if not value:
            return value
        return self.separator.join(value)

    def get_internal_type(self):
        return 'CharField'


class Repr:

    def _formatted_attrs(self):
        return ['id']

    def __repr__(self):
        class_name = self.__class__.__name__
        seen_attrs = self._formatted_attrs()
        attr_value = [f'[{key}={getattr(self, key)}]' for key in seen_attrs]
        attr_value = " ".join(attr_value)
        return f'<{class_name} {attr_value}>'


class User(AbstractUser):
    pass


class Game(Repr, models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    started_on = models.DateTimeField(auto_now_add=True, auto_now=False)
    won = models.BooleanField(default=False)
    secret_code = CommaSepField(max_length=512)

    def _formatted_attrs(self):
        return ['id', 'user']

    @property
    def moves(self):
        return Move.objects.filter(game=self).order_by('index').all()

    def add_move(self, move):
        move.game = self
        last_index = (
            Move
            .objects
            .filter(game=self)
            .aggregate(models.Max('index'))
        ).get('index__max')
        if last_index is None:
            last_index = 0
        move.index = last_index + 1
        move.save()


class Move(Repr, models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    index = models.SmallIntegerField()
    pegs = CommaSepField(max_length=512)
    result = CommaSepField(max_length=512)

    def _formatted_attrs(self):
        return ['id', 'game', 'pegs']
from django.contrib.admin import ModelAdmin, register
from django.contrib.auth.admin import UserAdmin
from main.models import *


@register(User)
class MainUserAdmin(UserAdmin):
    pass


@register(Game)
class GameAdmin(ModelAdmin):
    pass


@register(Move)
class MoveAdmin(ModelAdmin):
    pass
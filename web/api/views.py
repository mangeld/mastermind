from rest_framework import mixins
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import permissions

from main.models import Game, Move
from api.serializers import GameSerializer, MoveSerializer, PegColoursSerializer
from mastermind.peg import Peg


class GameList(
            mixins.ListModelMixin,
            mixins.CreateModelMixin,
            generics.GenericAPIView):
    """
    get:
    Returns a list of games for the current user

    post:
    Creates a game for the current user
    """

    queryset = Game.objects.all()
    serializer_class = GameSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request):
        queryset = self.get_queryset().filter(user=request.user)
        serializer = self.get_serializer_class()
        return Response(serializer(queryset, many=True).data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class MoveDetail(
    mixins.CreateModelMixin,
    generics.GenericAPIView):
    """
    post:
    Makes a guess, pegs should be an array of four (4) colours.
    Which should be one of the colours returned by `/v1/peg/colours`

    The result field indicates if there's coincidences, WHITE means
    a peg is found with the same color in the same position and RED means
    a peg is found with the same color in other position.
    """

    queryset = Move.objects.all()
    serializer_class = MoveSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class PegColoursDetail(generics.GenericAPIView):
    """
    Lists available colours to use when guessing a 
    secret code.
    """
    serializer_class = PegColoursSerializer

    def get(self, request, *args, **kwargs):
        data = {'colours': Peg.COLOURS}
        serializer = PegColoursSerializer(data=data)
        serializer.is_valid()
        return Response(serializer.data)
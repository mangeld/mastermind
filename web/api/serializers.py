from rest_framework import serializers
from main.models import Game, Move
from mastermind.peg import Peg
from mastermind.code_maker import RandomCodeMaker, StaticCodeMaker
from mastermind.board import Board, Rules
from mastermind.errors import MaxMovesReached


class ListInListValidator:

    def __init__(self, valid_choices):
        self.valid_choices = valid_choices

    def __call__(self, choices):
        for choice in choices:
            if choice not in self.valid_choices:
                raise serializers.ValidationError(
                    f"You must provide one of : {self.valid_choices}"
                )


class MoveSerializer(serializers.ModelSerializer):
    pegs = serializers.ListField(
        child=serializers.CharField(),
        min_length=4,
        max_length=4,
        validators=[ListInListValidator(Peg.COLOURS)],
    )
    result = serializers.SerializerMethodField()

    class Meta:
        model = Move
        fields = ('pegs', 'game', 'index', 'result',)
        read_only_fields = ('index',)

    def get_result(self, instance):
        mapping = {
            'False': 'RED',
            'True': 'WHITE',
            'None': '',
        }
        return [mapping.get(i, '') for i in instance.result]

    def create(self, validated_data):
        game = validated_data.get('game')
        pegs = validated_data.get('pegs')
        code = StaticCodeMaker(Rules(), [Peg(i) for i in game.secret_code])
        board = Board(code, Rules())
        result = board.guess([Peg(i) for i in pegs])
        move = Move(pegs=pegs, result=[str(i) for i in result])
        game.add_move(move)
        if board.won:
            game.won = board.won
            game.save()
        return move

    def move_to_pegs(self, move):
        return [Peg(color) for color in move]

    def validate(self, data):
        pegs = data.get('pegs')
        game = data.get('game')
        request = self.context.get('request', None)
        if request and request.user != game.user:
            raise serializers.ValidationError(
                "You cannot write to this game."
            )
        if game.won:
            raise serializers.ValidationError(
                "Game is already won!"
            )
        code = StaticCodeMaker(Rules(), [Peg(i) for i in game.secret_code])
        board = Board(
            code,
            Rules(),
            moves=[self.move_to_pegs(i.pegs) for i in game.moves]
        )
        try:
            board.guess([Peg(i) for i in pegs])
        except MaxMovesReached:
            raise serializers.ValidationError(
                "Max moves reached, try another game!"
            )

        return data


class GameSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    moves = MoveSerializer(many=True, read_only=True)
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    def create(self, validated_data):
        code = RandomCodeMaker.new().create_code()
        validated_data['secret_code'] = [i.color for i in code.pegs]
        return Game.objects.create(**validated_data)

    class Meta:
        model = Game
        fields = ('id', 'moves', 'started_on', 'user', 'won')
        read_only_fields = ('won',)


class PegColoursSerializer(serializers.Serializer):
    colours = serializers.ListField(
        child=serializers.CharField()
    )
from django.contrib import admin
from django.urls import path, include, reverse_lazy
from django.views.generic import RedirectView
from api import views
from rest_framework.documentation import include_docs_urls
from django.views.generic import RedirectView

API_TITLE = 'Mastermind API'
API_DESCRIPTION = 'Simple api for playing mastermind.'

urlpatterns = [
    path('game/', views.GameList.as_view(), name='game_list'),
    path('move/', views.MoveDetail.as_view(), name='move_detail'),
    path('peg/colours', views.PegColoursDetail.as_view(), name='peg_colours'),
    path('docs/', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)),
    path('', RedirectView.as_view(pattern_name='game_list', permanent=False)),
]
from typing import List
from .peg import Peg
from .utils import Random
from .rules import Rules
from dataclasses import dataclass


@dataclass
class SecretCode:
    pegs: List[Peg]


@dataclass
class CodeMaker:
    rules: Rules


@dataclass
class RandomCodeMaker(CodeMaker):
    random: Random

    def create_code(self) -> SecretCode:
        pegs: List[Peg] = []
        for i in range(self.rules.played_pegs):
            color = self.random.choice(Peg.COLOURS)
            peg = Peg(color)
            pegs.append(peg)
        return SecretCode(pegs)

    @classmethod
    def new(cls):
        return cls(Rules(), Random())


@dataclass
class StaticCodeMaker(CodeMaker):
    pegs: List[Peg]

    def create_code(self) -> SecretCode:
        if len(self.pegs) > self.rules.played_pegs:
            raise Exception()
        return SecretCode(self.pegs)

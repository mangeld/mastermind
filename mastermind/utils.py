from typing import TypeVar, Sequence
import random


T = TypeVar('T')


class Random:

    @classmethod
    def seed(cls, seed: int) -> None:
        random.seed(seed)

    @classmethod
    def choice(cls, items: Sequence[T]) -> T:
        return random.choice(items)

from typing import List, Union
from dataclasses import dataclass, field
from .code_maker import SecretCode
from .peg import Peg
from .rules import Rules
from .errors import MaxMovesReached
from copy import copy


def empty_list() -> List:
    return []


@dataclass
class Board:

    secret_code: SecretCode
    rules: Rules
    moves: List[List[Peg]] = field(default_factory=empty_list)

    def guess(self, guess: List[Peg]) -> List[Union[bool, None]]:
        if len(self.moves) == self.rules.max_guesses:
            raise MaxMovesReached
        self.moves.append(guess)
        return self._check_guess(guess)

    def _check_same_place(
            self,
            guess: List[Peg],
            secret_pegs: List[Peg]) -> List[bool]:
        result: List[bool] = []
        discovered_indexes: List[int] = []
        for i, peg in enumerate(guess):
            secret_peg = secret_pegs[i]
            if secret_peg == peg:
                result.append(True)
                discovered_indexes.append(i)
        for i in sorted(discovered_indexes, reverse=True):
            secret_pegs.pop(i)
        return result

    def _guess_in_secret(
            self,
            guess: List[Peg],
            secret_pegs: List[Peg]) -> List[bool]:
        result: List[bool] = []
        for peg in guess:
            if peg in secret_pegs:
                secret_pegs.pop(secret_pegs.index(peg))
                result.append(False)
        return result

    def _check_guess(self, guess: List[Peg]) -> List[Union[bool, None]]:
        answers: List[Union[bool, None]] = []
        secret_copy = copy(self.secret_code.pegs)
        correct_places: List[bool] = self._check_same_place(guess, secret_copy)
        correct_colors: List[bool] = self._guess_in_secret(guess, secret_copy)
        answers.extend(correct_places)
        answers.extend(correct_colors)
        while len(answers) < self.rules.played_pegs:
            answers.append(None)
        return answers

    @property
    def won(self):
        for guess in self.moves:
            if all(self._check_guess(guess)):
                return True
        return False

from dataclasses import dataclass
from typing import ClassVar, Any


@dataclass
class Peg:

    color: str
    COLOURS: ClassVar = [
        'RED', 'BLUE', 'GREEN', 'BLACK',
        'YELLOW', 'ORANGE', 'BROWN', 'WHITE',
    ]

    def __eq__(self, other: Any) -> bool:
        return other.color == self.color

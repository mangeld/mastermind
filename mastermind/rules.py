from typing import Set
from dataclasses import dataclass, field


def default_colours() -> Set[str]:
    return set([
        'RED', 'BLUE', 'GREEN',
        'BLACK', 'YELLOW', 'ORANGE',
        'BROWN', 'WHITE',
    ])


@dataclass
class Rules:

    max_guesses: int = 6
    played_pegs: int = 4
    colours: Set[str] = field(default_factory=default_colours)

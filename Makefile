.PHONY: lint hint pytest test

clean:
	find -iname '*pyc' -delete

lint:
	python -m flake8 mastermind test

static-analysis:
	python -m mypy --ignore-missing-imports mastermind test

pytest:
	python -m pytest --cov=mastermind -s -v test
	
test: clean lint static-analysis pytest
